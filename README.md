
博客：

[springboot（一）：项目创建](https://blog.csdn.net/stonezry/article/details/106050833)

[springboot（二）：数据库mysql8.0.19的下载安装](https://blog.csdn.net/stonezry/article/details/106057223)

[springboot（三）：数据库的配置（druid和mybatis）](https://blog.csdn.net/stonezry/article/details/106058781)

[springboot（四）：redis的配置和redis工具类封装](https://blog.csdn.net/stonezry/article/details/106076303)

[springboot（五）：采用RSA对用户登录凭证token进行生成和解析](https://blog.csdn.net/stonezry/article/details/106091029)

[springboot（六）：拦截器的设置](https://blog.csdn.net/stonezry/article/details/106092887)

[springboot（七）：设置全局异常处理](https://blog.csdn.net/stonezry/article/details/106094693)