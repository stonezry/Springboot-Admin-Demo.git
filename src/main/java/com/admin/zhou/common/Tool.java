package com.admin.zhou.common;

import com.admin.zhou.utils.JSONChange;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class Tool {


    public static void responeTo(HttpServletResponse response, String str) throws IOException {
        response.setCharacterEncoding("utf-8");
        PrintWriter out = response.getWriter();
        response.setContentType("application/json; charset=utf-8");
        ServerResponse responseStr = ServerResponse.createByNeedLogin(str);
        out.print(JSONChange.objToJson(responseStr));
        out.flush();
        out.close();
    }



}
