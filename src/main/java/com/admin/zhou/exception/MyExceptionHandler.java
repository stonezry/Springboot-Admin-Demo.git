package com.admin.zhou.exception;

import com.admin.zhou.common.ServerResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;

/**
 * 全局捕获异常
 */
@RestControllerAdvice
public class MyExceptionHandler {

    private static final Logger logger = LoggerFactory.getLogger(MyExceptionHandler.class);


    @ExceptionHandler(value=Exception.class)
    Object handleException(Exception e, HttpServletRequest request){

        logger.error("url:{},msg{}:",request.getRequestURL(),e.getMessage());
        return ServerResponse.createByError("数据异常",request.getRequestURI());
    }
}
