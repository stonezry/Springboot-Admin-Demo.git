package com.admin.zhou.config;

import com.admin.zhou.interceptor.AdminIntercepter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebMvcConfig implements WebMvcConfigurer {


    @Autowired
    private AdminIntercepter adminIntercepter;



    @Override
    public void addInterceptors(InterceptorRegistry registry) {

        registry.addInterceptor(adminIntercepter).addPathPatterns("/api/**").excludePathPatterns("/api/userLogin");

    }


}
