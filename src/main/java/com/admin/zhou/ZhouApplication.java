package com.admin.zhou;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@EnableTransactionManagement
@SpringBootApplication
public class ZhouApplication {

    public static void main(String[] args) {
        SpringApplication.run(ZhouApplication.class, args);
    }

}
