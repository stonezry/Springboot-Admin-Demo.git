package com.admin.zhou.model;


import java.util.List;


/**
 * @author: zhouruiyong
 * @Date: 2019/12/7
 * Describe: 权限实体类
 * @return
 */
public class TRights {

    /**
     * id
     */
    private Long id;


    /**
     * 菜单名称
     */
    private String name;


    /**
     * 路径
     */
    private String path;

    private int level;

    private List<TRights> children;


    /**
     * 父类id
     */
    private long pid;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public long getPid() {
        return pid;
    }

    public void setPid(long pid) {
        this.pid = pid;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public List<TRights> getChildren() {
        return children;
    }

    public void setChildren(List<TRights> children) {
        this.children = children;
    }
}
