package com.admin.zhou.model;


import java.util.List;


/**
 * @author: zhouruiyong
 * @Date: 2019/12/7
 * Describe: 角色实体类
 * @return
 */
public class TRoles {

    /**
     * id
     */
    private Long id;


    /**
     * 角色名称
     */
    private String roleName;


    /**
     * 角色描述
     */
    private String roleDescribe;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getRoleDescribe() {
        return roleDescribe;
    }

    public void setRoleDescribe(String roleDescribe) {
        this.roleDescribe = roleDescribe;
    }

}
