package com.admin.zhou.controller;

import com.admin.zhou.redis.RedisUtil;
import com.alibaba.druid.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

    @Autowired
    public RedisUtil redisUtil;


    @RequestMapping("/getReidsTest")
    public String getReidsTest(){

        String testStr = (String) redisUtil.get("test");

        if (StringUtils.isEmpty(testStr)){
            redisUtil.set("test","content");
            return "还未加入redis，第一次加入";
        }
        return "redis缓存内容："+testStr;
    }




    @RequestMapping("/")
    public String hello(){
        return "hello,spring boot!";
    }


    @RequestMapping("/testException")
    public String testException(){
        if(true) throw new RuntimeException("数据异常");
        return "hello,spring boot!";
    }
}
