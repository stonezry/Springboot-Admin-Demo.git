package com.admin.zhou.mapper;

import com.admin.zhou.model.TRights;
import com.admin.zhou.model.TAdminUser;
import com.admin.zhou.model.TRoles;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;


/**
 * @author: zhouruiyong
 * @Date: 2019/12/7
 * Describe: 管理用户表mapper
 */
@Mapper
@Repository
public interface TAdminUserMapper {


    @Select("select * from t_admin_user where account=#{account}")
    TAdminUser getAdminUserByAcount(String account);

    @Update("update t_admin_user set lastLoginTime=#{lastLoginTime} where id=#{id}")
    void updateRecentlyLanded(Long id, Date lastLoginTime);

    @Select("select * from t_admin_user WHERE name LIKE CONCAT('%',#{query},'%') or mobile LIKE CONCAT('%',#{query},'%') order by createTime desc limit #{page},#{pageSize}")
    List<TAdminUser> getUserList(String query,Integer page, Integer pageSize);

    @Select("select count(*) from t_admin_user WHERE name LIKE CONCAT('%',#{query},'%') or mobile LIKE CONCAT('%',#{query},'%')")
    int getUserToalNumber(String query);


    @Update("update t_admin_user set state=#{state} where id=#{id}")
    int updateUserState(int state,Long id);

    @Update("update t_admin_user set role_id=#{roleId} where id=#{id}")
    int updateUserRole(Long roleId,Long id);

    @Insert("insert into t_admin_user(name,mobile,account,password,role_id,state,createTime) values(#{name},#{mobile},#{account},#{password},#{role_id},#{state},#{createTime})")
    @Options(useGeneratedKeys=true, keyProperty="id", keyColumn="id")
    void addUser(TAdminUser user);


    @Select("select * from t_admin_user where id=#{id}")
    TAdminUser getAdminUserById(Long id);

    @Update("update t_admin_user set name=#{name},mobile=#{mobile} where id=#{id}")
    int updateUser(Long id,String name,String mobile);

    @Delete("delete from t_admin_user where id=#{id}")
    int deleteUserById(Long id);



    /**  权限  **/

    @Select("select * from t_rights order by pid")
    List<TRights> getRights();


    /**  角色  **/

    @Delete("delete from t_roles where id=#{id}")
    int deleteRoleById(Long id);

    @Insert("insert into t_roles(roleName,roleDescribe) values(#{roleName},#{roleDescribe})")
    @Options(useGeneratedKeys=true, keyProperty="id", keyColumn="id")
    void addRole(TRoles role);

    @Update("update t_roles set roleName=#{roleName},roleDescribe=#{roleDescribe} where id=#{id}")
    int updateRoleInfoById(Long id,String roleName,String roleDescribe);

    @Select("select * from t_roles WHERE id=#{id}")
    TRoles getRoleById(Long id);

    @Select("select * from t_roles")
    List<TRoles> getRoles();

    @Select("select r.* from t_rights as r join t_role_right as rr on r.id= rr.right_id AND rr.role_id=#{roleid}")
    List<TRights> getRightsByRoleId(Long roleid);

    @Delete("delete from t_role_right where role_id=#{roleid} and right_id=#{rightid}")
    int deleteRightOfRoleById(Long roleid,Long rightid);

    @Delete("delete from t_role_right where role_id=#{roleid}")
    int deleteRightByRoleId(Long roleid);

    @Select("select id from t_rights where id=#{id} or pid=#{id}")
    List<Long> getRightsByIdOrPid(Long id);

    @DeleteProvider(type = Provider.class, method = "batchDeleteRights")
    int deleteRightsByIds(@Param("ids") String ids,@Param("roleid") Long roleid);



    @Insert({
            "<script>",
            "insert into t_role_right(role_id,right_id) values ",
            "<foreach collection='ids' item='item' index='index' separator=','>",
            "(#{roleId}, #{item})",
            "</foreach>",
            "</script>"
    })
    int insertRightsToRole(@Param(value="ids") List<String> ids,@Param(value="roleId") Long roleId);


    class Provider {

        /* 批量删除权限 */
        public String batchDeleteRights(String ids,Long roleid) {
            StringBuilder sb = new StringBuilder();
            sb.append("DELETE FROM t_role_right WHERE role_id="+roleid+" AND right_id IN ("+ids+")");
            return sb.toString();
        }
    }
}
