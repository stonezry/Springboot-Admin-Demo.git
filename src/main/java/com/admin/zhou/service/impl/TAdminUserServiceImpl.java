package com.admin.zhou.service.impl;

import com.admin.zhou.mapper.TAdminUserMapper;
import com.admin.zhou.model.TRights;
import com.admin.zhou.model.TAdminUser;
import com.admin.zhou.model.TRoles;
import com.admin.zhou.service.TAdminUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;


@Service
public class TAdminUserServiceImpl implements TAdminUserService {

    @Autowired
    private TAdminUserMapper tAdminUserMapper;


    @Override
    public TAdminUser getAdminUserByAcount(String account) {
        return tAdminUserMapper.getAdminUserByAcount(account);
    }

    @Override
    public void updateRecentlyLanded(Long id, Date lastLoginTime) {
        tAdminUserMapper.updateRecentlyLanded(id,lastLoginTime);
    }

    @Override
    public List<TRights> getRights() {
        return tAdminUserMapper.getRights();
    }


    @Override
    public List<TRoles> getRoles() {
        return tAdminUserMapper.getRoles();
    }

    @Override
    public List<TRights> getRightsByRoleId(Long roleid) {
        return tAdminUserMapper.getRightsByRoleId(roleid);
    }

    @Override
    public List<Long> getRightsByIdOrPid(Long id) {
        return tAdminUserMapper.getRightsByIdOrPid(id);
    }

    @Override
    public int deleteRightOfRoleById(Long roleid, Long rightid) {
        return tAdminUserMapper.deleteRightOfRoleById(roleid,rightid);
    }

    @Override
    public int deleteRightByRoleId(Long roleid) {
        return tAdminUserMapper.deleteRightByRoleId(roleid);
    }

    @Override
    public int deleteRightsByIds(String ids, Long roleid) {
        return tAdminUserMapper.deleteRightsByIds(ids,roleid);
    }


    @Transactional
    @Override
    public int insertRightsToRole(List<String> ids, Long roleId) {

        tAdminUserMapper.deleteRightByRoleId(roleId);
        return tAdminUserMapper.insertRightsToRole(ids,roleId);
    }

    @Override
    public void addRole(TRoles role) {
        tAdminUserMapper.addRole(role);
    }

    @Override
    public int updateRoleInfoById(Long id, String roleName, String roleDescribe) {
        return tAdminUserMapper.updateRoleInfoById(id,roleName,roleDescribe);
    }

    @Override
    public int deleteRoleById(Long id) {
        return tAdminUserMapper.deleteRoleById(id);
    }

    @Override
    public List<TAdminUser> getUserList(String query,Integer page, Integer pageSize) {
        return tAdminUserMapper.getUserList(query,page,pageSize);
    }

    @Override
    public int getUserToalNumber(String query) {
        return tAdminUserMapper.getUserToalNumber(query);
    }



    @Override
    public int updateUserState(int state, Long id) {
        return tAdminUserMapper.updateUserState(state,id);
    }

    @Override
    public void addUser(TAdminUser user) {
        tAdminUserMapper.addUser(user);
    }

    @Override
    public TAdminUser getAdminUserById(Long id) {
        return tAdminUserMapper.getAdminUserById(id);
    }

    @Override
    public int updateUser(Long id, String name, String mobile) {
        return tAdminUserMapper.updateUser(id,name,mobile);
    }

    @Override
    public int deleteUserById(Long id) {
        return tAdminUserMapper.deleteUserById(id);
    }

    @Override
    public int updateUserRole(Long roleId, Long id) {
        return tAdminUserMapper.updateUserRole(roleId,id);
    }

    @Override
    public TRoles getRoleById(Long id) {
        return tAdminUserMapper.getRoleById(id);
    }
}
