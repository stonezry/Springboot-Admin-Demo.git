package com.admin.zhou.service;

import com.admin.zhou.model.TRights;
import com.admin.zhou.model.TAdminUser;
import com.admin.zhou.model.TRoles;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
 * @author: zhou
 * @Date: 2019/2/7
 * Describe: 用户业务操作
 * @return
 */
public interface TAdminUserService {

    //根据账号获取用户
    TAdminUser getAdminUserByAcount(String account);

    void updateRecentlyLanded(Long id, Date lastLoginTime);


    List<TAdminUser> getUserList(String query,Integer page, Integer pageSize);

    int getUserToalNumber(String query);


    //更新用户状态
    int updateUserState(int state,Long id);

    void addUser(TAdminUser user);

    TAdminUser getAdminUserById(Long id);

    int updateUser(Long id,String name,String mobile);

    int deleteUserById(Long id);

    int updateUserRole(Long roleId,Long id);


    //权限
    List<TRights> getRights();


    //角色
    TRoles getRoleById(Long id);

    List<TRoles> getRoles();

    List<TRights> getRightsByRoleId(Long roleid);

    List<Long> getRightsByIdOrPid(Long id);

    int deleteRightOfRoleById(Long roleid,Long rightid);

    int deleteRightByRoleId(Long roleid);

    int deleteRightsByIds(String ids, Long roleid);

    int insertRightsToRole(List<String> ids,Long roleId);

    void addRole(TRoles role);
    int updateRoleInfoById(Long id,String roleName,String roleDescribe);
    int deleteRoleById(Long id);
}
